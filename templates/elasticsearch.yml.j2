################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

################################### Cluster ###################################

# Cluster name identifies your cluster for auto-discovery. If you're running
# multiple clusters on the same network, make sure you're using unique names.
#
{% if es_cluster_name is defined %}
cluster.name: {{ es_cluster_name }}
{% endif %}

#################################### Node #####################################

# Node names are generated dynamically on startup, so you're relieved
# from configuring them manually. You can tie this node to a specific name:
#
{% if es_node_name is defined %}
node.name: {{es_node_name}}
{% elif ansible_virtualization_type is defined %} 
{% if ansible_virtualization_type == 'virtualbox' %}
# if in vagrant, use ansible_hostname as nodename
node.name: {{ ansible_hostname }}
{% endif %}
{% elif ansible_fqdn is defined %}
node.name: {{ ansible_fqdn }}
{% elif ansible_hostname is defined %}
node.name: {{ ansible_hostname }}
{% endif %}


# Every node can be configured to allow or deny being eligible as the master,
# and to allow or deny to store the data.
{% if es_node_master is defined %}
node.master: {{ es_node_master }}
{% endif %}
{% if es_node_data is defined %}
node.data: {{ es_node_data }}
{% endif %}



# Use the Cluster Health API [http://localhost:9200/_cluster/health], the
# Node Info API [http://localhost:9200/_nodes] or GUI tools
# such as <http://www.elasticsearch.org/overview/marvel/>,
# <http://github.com/karmi/elasticsearch-paramedic>,
# <http://github.com/lukas-vlcek/bigdesk> and
# <http://mobz.github.com/elasticsearch-head> to inspect the cluster state.

# A node can have generic attributes associated with it, which can later be used
# for customized shard allocation filtering, or allocation awareness. An attribute
# is a simple key value pair, similar to node.key: value, here is an example:
#
#node.rack: rack314

# By default, multiple nodes are allowed to start from the same installation location
# to disable it, set the following:
#node.max_local_storage_nodes: 1


#################################### Index ####################################

# You can set a number of options (such as shard/replica options, mapping
# or analyzer definitions, translog settings, ...) for indices globally,
# in this file.

{% if es_number_shards is defined %}
index.number_of_shards: {{ es_number_shards }}
{% endif %}
{% if es_number_replicas is defined%}
index.number_of_replicas: {{ es_number_replicas }}
{% endif %}


#################################### Paths ####################################

# Path to directory containing configuration (this file and logging.yml):
{% if data_disk is defined %}
path.data: /data
{% endif %}

# Can optionally include more than one location, causing data to be striped across
# the locations (a la RAID 0) on a file level, favouring locations with most free
# space on creation. For example:
#
#path.data: /path/to/data1,/path/to/data2

# Path to temporary files:
#
#path.work: /path/to/work

# Path to log files:
#
#path.logs: /path/to/logs

# Path to where plugins are installed:
#
#path.plugins: /path/to/plugins


#################################### Plugin ###################################

# If a plugin listed here is not installed for current node, the node will not start.
#plugin.mandatory: mapper-attachments,lang-groovy


################################### Memory ####################################

# Elasticsearch performs poorly when JVM starts swapping: you should ensure that
# it _never_ swaps.

# Set this property to true to lock the memory:
{% if es_bootstrap_mlockall is defined %}
bootstrap.mlockall: {{ es_bootstrap_mlockall }}
{% endif %}


############################## Network And HTTP ###############################

# Elasticsearch, by default, binds itself to the 0.0.0.0 address, and listens
# on port [9200-9300] for HTTP traffic and on port [9300-9400] for node-to-node
# communication. (the range means that if the port is busy, it will automatically
# try the next port).

# Set the bind address specifically (IPv4 or IPv6):
#
#network.bind_host: 192.168.0.1

# Set the address other nodes will use to communicate with this node. If not
# set, it is automatically derived. It must point to an actual IP address.
#
#network.publish_host: 192.168.0.1

# Set both 'bind_host' and 'publish_host':
#
#network.host: 192.168.0.1

# Set a custom port for the node to node communication (9300 by default):
#
#transport.tcp.port: 9300

# Enable compression for all communication between nodes (disabled by default):
#
#transport.tcp.compress: true

# Set a custom port to listen for HTTP traffic:
#
#http.port: 9200

# Set a custom allowed content length:
#
#http.max_content_length: 100mb

# Disable HTTP completely:
#
#http.enabled: false

{% if ansible_ethwe is defined %}
network.host: _ethwe:ipv4_
{% elif ansible_virtualization_type is defined %}
{% if ansible_virtualization_type == 'virtualbox' %}
# set up network to use eth1 instead of the default eth0 in vagrant
network.host: _eth1:ipv4_
{% endif %}
{% endif %}

################################### Gateway ###################################

# The gateway allows for persisting the cluster state between full cluster
# restarts. Every change to the state (such as adding an index) will be stored
# in the gateway, and when the cluster starts up for the first time,
# it will read its state from the gateway.

# There are several types of gateway implementations. For more information, see
# <http://elasticsearch.org/guide/en/elasticsearch/reference/current/modules-gateway.html>.

# The default gateway type is the "local" gateway (recommended):
#
#gateway.type: local

# Settings below control how and when to start the initial recovery process on
# a full cluster restart (to reuse as much local data as possible when using shared
# gateway).

# Allow recovery process after N nodes in a cluster are up:
#
#gateway.recover_after_nodes: 1

# Set the timeout to initiate the recovery process, once the N nodes
# from previous setting are up (accepts time value):
#
#gateway.recover_after_time: 5m

# Set how many nodes are expected in this cluster. Once these N nodes
# are up (and recover_after_nodes is met), begin recovery process immediately
# (without waiting for recover_after_time to expire):
#
#gateway.expected_nodes: 2


############################# Recovery Throttling #############################

# These settings allow to control the process of shards allocation between
# nodes during initial recovery, replica allocation, rebalancing,
# or when adding and removing nodes.

# Set the number of concurrent recoveries happening on a node:
#
# 1. During the initial recovery
#
#cluster.routing.allocation.node_initial_primaries_recoveries: 4
#
# 2. During adding/removing nodes, rebalancing, etc
#
#cluster.routing.allocation.node_concurrent_recoveries: 2

# Set to throttle throughput when recovering (eg. 100mb, by default 20mb):
#
#indices.recovery.max_bytes_per_sec: 20mb

# Set to limit the number of open concurrent streams when
# recovering a shard from a peer:
#
#indices.recovery.concurrent_streams: 5


################################## Discovery ##################################

# Discovery infrastructure ensures nodes can be found within a cluster
# and master node is elected. Multicast discovery is the default.

{% if es_discovery_zen_minimum_master_nodes is defined %}
discovery.zen.minimum_master_nodes: {{ es_discovery_zen_minimum_master_nodes }}
{% endif %}

# Set the time to wait for ping responses from other nodes when discovering.
# Set this option to a higher value on a slow or congested network
# to minimize discovery failures:
#
#discovery.zen.ping.timeout: 3s

# EC2 discovery allows to use AWS EC2 API in order to perform discovery.
{% if es_multicast_enabled == true %}
discovery.zen.ping.multicast.enabled: {{ es_multicast_enabled }}
{% else %}
discovery.zen.ping.multicast.enabled: false
discovery.zen.ping.unicast.hosts: {{ ":9300, ".join((hostvars['localhost']['elasticsearch_private_ips']).split(',')) }}:9300
{% endif %}

{% if es_bucket is defined %}
cloud.aws.access_key: {{ es_access_key }}
cloud.aws.secret_key: {{ es_secret_key }}
repositories:
  s3:
    bucket: {{ es_bucket }}
    region: {{ es_region }}
{% endif %}

################################## Slow Log ##################################

# Shard level query and fetch threshold logging.

#index.search.slowlog.threshold.query.warn: 10s
#index.search.slowlog.threshold.query.info: 5s
#index.search.slowlog.threshold.query.debug: 2s
#index.search.slowlog.threshold.query.trace: 500ms

#index.search.slowlog.threshold.fetch.warn: 1s
#index.search.slowlog.threshold.fetch.info: 800ms
#index.search.slowlog.threshold.fetch.debug: 500ms
#index.search.slowlog.threshold.fetch.trace: 200ms

#index.indexing.slowlog.threshold.index.warn: 10s
#index.indexing.slowlog.threshold.index.info: 5s
#index.indexing.slowlog.threshold.index.debug: 2s
#index.indexing.slowlog.threshold.index.trace: 500ms

################################## GC Logging ################################

#monitor.jvm.gc.young.warn: 1000ms
#monitor.jvm.gc.young.info: 700ms
#monitor.jvm.gc.young.debug: 400ms

#monitor.jvm.gc.old.warn: 10s
#monitor.jvm.gc.old.info: 5s
#monitor.jvm.gc.old.debug: 2s
{% if es_threadpool_bulk_type is defined %}
threadpool.bulk.type: {{ es_threadpool_bulk_type }}
{% endif %}
{% if es_threadpool_bulk_size is defined %}
threadpool.bulk.size: {{ es_threadpool_bulk_size }}
{% endif %}
{% if es_threadpool_bulk_queue_size is defined %}
threadpool.bulk.queue_size: {{ es_threadpool_bulk_queue_size }}
{% endif %}


################################## Security ################################

# Uncomment if you want to enable JSONP as a valid return transport on the
# http server. With this enabled, it may pose a security risk, so disabling
# it unless you need it is recommended (it is disabled by default).
#
#http.jsonp.enable: true
{% if es_auto_create_index is defined %}
# allow indexes to be created with the name matching 
# or false to disable automatic creation
action.auto_create_index: {{ es_auto_create_index }}
{% endif %}
#dynamic scripting
{% if es_disable_dynamic is defined %}
script.disable_dynamic: {{ es_disable_dynamic }}
{% endif %}



################################## Plugins Config ################################

{% if plugins is defined and 'marvel' in plugins %}
{% include 'includes/marvel.elasticsearch.yml.j2' %}
{% endif %}

{% if plugins is defined and 'update' in plugins %}
{% include 'includes/update_by_query.elasticsearch.yml.j2' %}
{% endif %}

{% if machine_running_ssd is defined and machine_running_ssd == false %}
index.merge.scheduler.max_thread_count: 1
indices.store.throttle.max_bytes_per_sec : "20mb"
{% elif machine_running_ssd is defined and machine_running_ssd == true %}
indices.store.throttle.max_bytes_per_sec : "100mb"
{% endif %}
