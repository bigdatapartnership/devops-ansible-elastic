# Big Data Partnership - Elastic Ansible setup

This set of Ansible scripts has been designed to be run against a set of configured CentOS 6 servers, these can either be the Big Data Partnership docker containers or can be real / cloud servers running the latest updates to CentOS 6.

For the setup to work please update the hosts file with the correct IP Addresses to access the server and ensure the vars.yml file also has the correct details for your environment. Once that is done you can use the make commands to install an Elastic cluster.


    make provision

    ################################################################################
    #   Copyright 2015 Big Data Partnership Ltd                                    #
    #                                                                              #
    #   Licensed under the Apache License, Version 2.0 (the "License");            #
    #   you may not use this file except in compliance with the License.           #
    #   You may obtain a copy of the License at                                    #
    #                                                                              #
    #       http://www.apache.org/licenses/LICENSE-2.0                             #
    #                                                                              #
    #   Unless required by applicable law or agreed to in writing, software        #
    #   distributed under the License is distributed on an "AS IS" BASIS,          #
    #   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
    #   See the License for the specific language governing permissions and        #
    #   limitations under the License.                                             #
    ################################################################################
# Elasticsearch Specific Settings


## EC2 Permissions for user to do dynamic service discovery
{
    "Statement": [
        {
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Effect": "Allow",
            "Resource": [
                "*"
            ]
        }
    ],
    "Version": "2012-10-17"
}


## EC2 Permissions for user to backup to s3

Create the bucket with the following:
$ curl -XPUT 'http://localhost:9200/_snapshot/my_s3_repository' -d '{
    "type": "s3",
    "settings": {
        "bucket": "my_bucket_name",
        "region": "us-east"
    }
}'


AWS credentials:
{
    "Statement": [
        {
            "Action": [
                "s3:ListBucket"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::snaps.example.com"
            ]
        },
        {
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::snaps.example.com/*"
            ]
        }
    ],
    "Version": "2012-10-17"
}